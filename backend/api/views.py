from rest_framework.viewsets import ModelViewSet
from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db.models import Q
from django_filters import rest_framework as filters


from .models import Task


class TaskFilter(filters.FilterSet):
    search_query = filters.CharFilter(method="filter_by_search_query")

    class Meta:
        model = Task
        fields = ["search_query"]
    
    def filter_by_search_query(self, queryset, __, value):
        return queryset.filter(Q(name__icontains=value) | Q(details__icontains=value))


class TaskViewSet(ModelViewSet):
    queryset = Task.objects.all().order_by("-id")
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TaskFilter

    class TaskSerializer(serializers.ModelSerializer):
        class Meta:
            model = Task
            fields = "__all__"

    def get_serializer_class(self):
        return self.TaskSerializer
    
    @action(detail=True, methods=["post"], url_path="update")
    def update_task(self, request, pk):
        instance = self.get_object()
        serializer = self.get_serializer_class()(instance=instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)