import {
  Text,
  Button,
  Input,
  FormControl,
  FormLabel,
  Textarea,
} from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import useTodoStore from "../stores/TodoStore";
import React from "react";
import { format } from "date-fns";
import { useFormik } from "formik";
import * as Yup from "yup";

interface IProps {
  onClose: () => void;
}

function CreateUpdateModal({ onClose }: IProps) {
  const { task, saveTask } = useTodoStore();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);

  const formik = useFormik({
    initialValues: {
      name: task?.name ?? "",
      details: task?.details ?? "",
      deadline: task?.deadline
        ? format(new Date(task.deadline), "yyyy-MM-dd")
        : format(new Date(), "yyyy-MM-dd"),
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .min(1, "Name of the task mustn't be empty")
        .required("Task name is required"),
      details: Yup.string()
        .min(20, "Enter more details (20 chars at least)")
        .required("Task details is required"),
      deadline: Yup.date().notRequired(),
    }),
    onSubmit: (values) => {
      saveTask(values);
      onClose();
    },
  });

  return (
    <Modal
      initialFocusRef={initialRef}
      finalFocusRef={finalRef}
      isOpen={true}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create a task</ModalHeader>
        <ModalCloseButton />

        <form onSubmit={formik.handleSubmit}>
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Name</FormLabel>
              <Input
                placeholder="Name"
                size="sm"
                name="name"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.name.toString()}
              />
              {formik.touched.name && formik.errors.name ? (
                <Text className="error">{formik.errors.name}</Text>
              ) : null}
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Details</FormLabel>
              <Textarea
                placeholder="Task details"
                size="sm"
                name="details"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.details.toString()}
              />
              {formik.touched.details && formik.errors.details ? (
                <Text className="error">{formik.errors.details}</Text>
              ) : null}
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Deadline</FormLabel>
              <Input
                size="sm"
                placeholder="yyyy-MM-dd"
                name="deadline"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.deadline}
              />
              {formik.touched.deadline && formik.errors.deadline ? (
                <Text className="error">{formik.errors.deadline}</Text>
              ) : null}
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button type="submit" colorScheme="blue" mr={3} size="sm">
              Save
            </Button>
            <Button onClick={onClose} size="sm">
              Cancel
            </Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
}

export default CreateUpdateModal;
