import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  Container,
  Text,
  Button,
  HStack,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import {
  AiFillCheckSquare,
  AiFillPlusCircle,
  AiOutlineCheckSquare,
} from "react-icons/ai";
import { RiEditBoxFill } from "react-icons/ri";
import useTodoStore, { ITask } from "../stores/TodoStore";
import React from "react";
import CreateUpdateModal from "./CreateUpdateModal";

function ListPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { tasks, setSearchQuery, setTask, fetchTasks, updateStatus } =
    useTodoStore();

  const handleSearch = React.useCallback((value: String) => {
    setSearchQuery(value);
    fetchTasks();
  }, []);

  const handleMarkDone = React.useCallback((task: ITask) => {
    updateStatus(task.id ?? -1, true);
  }, []);

  const handleUnmarkDone = React.useCallback((task: ITask) => {
    updateStatus(task.id ?? -1, false);
  }, []);

  React.useEffect(() => {
    fetchTasks();
  }, []);

  return (
    <Container maxW="90vw" overflowX="auto" p={4}>
      {isOpen ? <CreateUpdateModal onClose={onClose} /> : null}

      <HStack p="5px" justifyContent="space-between">
        <Input
          placeholder="Type to search..."
          size="sm"
          w="md"
          onChange={(e) => handleSearch(e.target.value)}
        />
        <Button colorScheme="blue" size="sm" onClick={onOpen}>
          <AiFillPlusCircle /> <Text ml="1">Create</Text>
        </Button>
      </HStack>
      <Table
        variant="simple"
        size="sm"
        __css={{ "table-layout": "fixed", width: "full" }}
        mt="4"
      >
        <Thead>
          <ColumnNames />
        </Thead>
        <Tbody>
          {tasks.map((task) => (
            <Tr key={task.id}>
              <Td>{task.name}</Td>
              <Td width="40%">{task.details}</Td>
              <Td>
                {task.status === true ? (
                  <HStack>
                    <AiFillCheckSquare className="icon green" />
                    <Button
                      size="xs"
                      colorScheme="green"
                      onClick={() => handleUnmarkDone(task)}
                    >
                      Unmark done
                    </Button>
                  </HStack>
                ) : (
                  <HStack>
                    <AiOutlineCheckSquare className="icon red" />
                    <Button
                      size="xs"
                      colorScheme="red"
                      onClick={() => handleMarkDone(task)}
                    >
                      Mark done
                    </Button>
                  </HStack>
                )}
              </Td>
              <Td>{task.deadline?.toString()}</Td>
              <Td>
                <Button
                  size="sm"
                  onClick={() => {
                    setTask(task);
                    onOpen();
                  }}
                >
                  <RiEditBoxFill />
                </Button>
              </Td>
            </Tr>
          ))}
        </Tbody>
        <Tfoot>
          <ColumnNames />
        </Tfoot>
      </Table>
    </Container>
  );
}

function ColumnNames() {
  return (
    <Tr>
      <Th>Name</Th>
      <Th width="50%">Details</Th>
      <Th>Status</Th>
      <Th>Deadline</Th>
      <Th></Th>
    </Tr>
  );
}

export default ListPage;
