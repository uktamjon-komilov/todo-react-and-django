import { ChakraProvider } from "@chakra-ui/react";
import ListPage from "./pages/ListPage";
import "react-datepicker/dist/react-datepicker-cssmodules.css";

function App() {
  return (
    <ChakraProvider>
      <ListPage />
    </ChakraProvider>
  );
}

export default App;
