from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=255)
    details = models.TextField()
    status = models.BooleanField(default=False)
    deadline = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name