import { create } from "zustand";
import { BASE_URL } from "../constants";

export interface ITask {
  id: number | null;
  name: String;
  details: String;
  status: boolean;
  deadline: Date | null;
}

export interface ITaskData {
  name: String;
  details: String;
  deadline: String;
}

interface ITodoStore {
  tasks: ITask[];
  task: ITask | null;
  searchQuery: String | null;
  setTask: (task: ITask) => void;
  setSearchQuery: (value: String) => void;
  fetchTasks: (page?: number) => Promise<void>;
  saveTask: (data: ITaskData) => Promise<void>;
  updateStatus: (id: number, status: boolean) => Promise<void>;
}

const useTodoStore = create<ITodoStore>((set, get) => ({
  tasks: [],
  task: null,
  searchQuery: "",
  currentPage: 1,
  totalPages: 1,
  setTask: (task) => {
    set({ task: task });
  },
  setSearchQuery: (value: String) => {
    set({ searchQuery: value });
  },
  fetchTasks: async (page = 1) => {
    const { searchQuery } = get();
    try {
      const response = await fetch(
        `${BASE_URL}/api/tasks/?page=${page}&search_query=${searchQuery}`
      );
      const data = await response.json();

      set({
        tasks: data,
      });
    } catch (error) {
      console.error("Error fetching tasks:", error);
    }
  },
  saveTask: async (data: ITaskData) => {
    const { task, fetchTasks } = get();
    console.log(data);
    try {
      if (task === null) {
        await fetch(`${BASE_URL}/api/tasks/`, {
          method: "post",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify(data),
        });
      } else {
        await fetch(`${BASE_URL}/api/tasks/${task.id}/update/`, {
          method: "post",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify(data),
        });
      }
      await fetchTasks();
    } catch (error) {
      console.error("Error fetching tasks:", error);
    }
  },
  updateStatus: async (id: number, status: boolean) => {
    const { fetchTasks } = get();
    try {
      await fetch(`${BASE_URL}/api/tasks/${id}/update/`, {
        method: "post",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          status: status,
        }),
      });
      await fetchTasks();
    } catch (error) {
      console.error("Error fetching tasks:", error);
    }
  },
}));

export default useTodoStore;
